function [sumareinas, tablero] = datos(inic, inif, reinas, sumareinas, tablero) 
	for c=inic:reinas %Recorre las columnas empezando por inic 
		colocado=0; %Var. para saber si ha colocado una reina  
		for f=inif:reinas %Recorre las filas empezando por inif  
			ocupadof=0; %Var. para saber si hay una reina en la misma fila   
			ocupadofc=0; %Var. para saber si hay reinas en la diagonal 
			for i=1:reinas %Busca reinas en la misma fila 
				if tablero (f,i)==1 
					ocupadof=1; 
					break 
				end 
			end 
			if ocupadof==0  
				for i=1:reinas-1 %Busca reinas en la diagonal (derecha, arriba) 
					if c-i<=0 
						break 
					end 
					if f-i<=0 
						break 
					end 
					if tablero (f-i,c-i)==1 
						ocupadofc=1; 
						break 
					end 
				end 
				for i=1:reinas-1 %Busca reinas en la diagonal (derecha, abajo)  
					if c-i<=0 
						break 
					end 
					if f+i>reinas 
						break 
					end 
					if tablero (f+i,c-i)==1 
						ocupadofc=1; 
						break 
					end 
				end 
				if ocupadofc==0 
					tablero (f,c)=1; %Coloca la reina 
					colocado=1; 
					sumareinas=sumareinas+1; 
					break 
				end 
			end 
		end 
		inif=1; 
		if colocado==0 
			break 
		end 
	end 
end 