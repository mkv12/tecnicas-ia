function [inic, inif, sumareinas, tablero] = arreglar(tablero, reinas, sumareinas) 
	encontrado=0; %Var. para saber si ha encontrado una reina 
	for c=-reinas:-1 %Busca la �ltima reina colocada 
		for f=1:reinas 
			if tablero (f,-c)==1 
				encontrado=1; 
				tablero (f,-c)=0; %Borra la �ltima reina 
				sumareinas=sumareinas-1; 
				inic=-c; %Cambia inic a la columna de la �ltima reina colocada 
				inif=f+1; %Cambia inif a la fila siguiente de la �ltima reina colocada 
				break 
			end 
		end 
		if encontrado==1 
			break 
		end 
	end 
end