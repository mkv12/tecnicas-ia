reinas=input('Número de reinas? '); 
todas=input('Buscar todas las soluciones? (Y/N) ','s'); %Y:devuelve todas las sol; N: devuelve las sol 1 x 1 
tic %Inicia el cron�metro 
s=0; %N� de sol encontradas 
fin=0; %Var. para el while 
inic=1; %Columna en la que empiezainif=1; %Fila en la que comienza 
tablero = zeros(reinas,reinas); %Crea una matriz nula de reinas x reinas 
sumareinas=0; %Cantidad de reinas colocadas en el tablero 

while fin==0 
  [sumareinas, tablero] = datos (inic, inif, reinas, sumareinas, tablero); 
  if sumareinas==reinas 
    s=s+1; 
    disp (' ') 
    disp(['Sol.:' num2str(s)]) 
    disp(tablero) 
    if upper(todas)=='N' 
      disp (' ') 
      toc 
      x=input('Siguiente soluci�n? (Y/N) ','s'); 
      if upper(x)=='N' 
        fin=1; 
        disp (' ') 
        disp ('Fin del programa') 
      else 
        tic 
      end 
    end 
  end 
  [inic, inif, sumareinas, tablero]=arreglar(tablero, reinas, sumareinas); 
  if inic==1 
    if inif>reinas 
      fin=1; 
      disp (' ') 
      disp ('Fin del programa') 
      if upper(todas)=='Y' 
      disp (' ') 
      toc 
      end 
    end 
  end 
end
