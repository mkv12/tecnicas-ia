clc 
close all
clear all
tic;
hold on
reinas=4;
x=-reinas:1:0;
yx=-reinas:1:0;
b=-reinas:0;
c=-reinas:0;
m=0;arbol={};visitado={};
%%%%%%%%%%%%%%%pinta el tablero
for i=1:length(b)
    y=m*x+b(i);
    plot(x,y);
    x1=m*yx+c(i);
    plot(x1,yx);
end
tablero = zeros(reinas,reinas);
%%%%%%%%%%%%%%%%%%
raiz=tablero;%nodo raiz
arbol(1)={raiz};%al principio de la lista
index_raiz=2;
index_vis=1;
flag=1;
inicio1=1;
inicio2=1;
while (flag==1)
    %colisiones y siguiente movimiento
    [iqx iqy]=find(9==tablero);%buscamos las reinas del tablero
    queen=[iqx iqy];%reinas del tablero
    
    if isempty(queen)%si no hay reinas
            if inicio2==reinas%iniciamos 1 reina en la primer casilla
                inicio1=inicio1+1;
                inicio2=1;
            end
            if inicio1==reinas
                inicio1=1;
            end
            tablero(inicio1,inicio2)=9;%la ponemos en el tablero
            arbol(index_raiz)={tablero};%la metemos en la pila
            visitado(index_vis)={tablero};
            
            index_raiz=index_raiz+1;
            index_vis=index_vis+1;
            
            index_i=inicio1;index_j=inicio2;
            posx=(-reinas+index_i)-.5;%pintamos en el tablero
            posy=-index_j+.5;
            text(posx,posy,'R');
            inicio2=inicio2+1;
            cond=0;
     else

        fou=0;dodiag=0;
        for i=1:reinas%recorremos todo el tablero
            for j=1:reinas
                cond=0;cont=0;
                move_done=0;%condicion para movimientos repetidos
                next_move=[i j];%siguiente posible jugada
                if dodiag==0 
                    diagonal=zeros(12*reinas,2);
                    for z=1:size(queen,1)
                        for m=1:reinas-1
                            diagonal(m+cont,1)=queen(z,1)+m;%diagonales de colision                        
                            diagonal(m+cont,2)=queen(z,2)+m;
                            diagonal(m+1+cont,1)=queen(z,1)-m;%diagonales de colision                        
                            diagonal(m+1+cont,2)=queen(z,2)-m;
                            diagonal(m+2+cont,1)=queen(z,1)-m;%diagonales de colision                        
                            diagonal(m+2+cont,2)=queen(z,2)+m;
                            diagonal(m+3+cont,1)=queen(z,1)+m;%diagonales de colision                        
                            diagonal(m+3+cont,2)=queen(z,2)-m;
                            cont=cont+reinas-1;
                        end
                        cont=cont+reinas-1;
                    end
                dodiag=1;
                end
                for d =1:size(next_move,1)%virificamos las colisiones
                    for c=1:length(diagonal)%si choca en diagonal
                        if next_move(d,1)==diagonal(c,1)...
                        && next_move(d,2)==diagonal(c,2)
                            cond=1;
                            index_i=next_move(1);index_j=next_move(2);
                            posx=(-reinas+index_i)-.5;
                            posy=-index_j+.5;
                            text(posx,posy,'C');
                            pause(.5);                            
                            text(posx,posy,'\color{white}\bfC','BackgroundColor',[1 1 1]);
                            [ix iy]=find(9==tablero);%pintamos en el tablero
                            queens_in=[ix iy];
                             for n=1:size(queens_in,1)
                                index_i=queens_in(n,1);index_j=queens_in(n,2);
                                posx=(-reinas+index_i)-.5;
                                posy=-index_j+.5;
                                text(posx,posy,'R');                
                            end
                            break;
                        end                                      
                    end
                    if cond==1
                        break;
                    end
                end
                for x=1:size(queen,1)
                    if next_move(1,1)==queen(x,1) || next_move(1,2)==queen(x,2)
                        cond=1;%si choca en fila o columna
                            index_i=next_move(1);index_j=next_move(2);
                            posx=(-reinas+index_i)-.5;
                            posy=-index_j+.5;
                            text(posx,posy,'C');
                            pause(.5);
                            text(posx,posy,'\color{white}\bfC','BackgroundColor',[1 1 1]);
                            [ix iy]=find(9==tablero);%pintamos en el tablero
                            queens_in=[ix iy];
                             for n=1:size(queens_in,1)
                                index_i=queens_in(n,1);index_j=queens_in(n,2);
                                posx=(-reinas+index_i)-.5;
                                posy=-index_j+.5;
                                text(posx,posy,'R');                
                            end
                    end
                    if cond==1
                        break;
                    end
                end

                if cond==1%si choca otra jugada
                    continue;
                else
                    tablero(next_move(1),next_move(2))=9;%si no la tomamos
                    move=tablero;
                    for t=1:length(visitado)%verificar si no es edo visitado
                        ya_visitado=cell2mat(visitado(t));
                        if ya_visitado == tablero
                            move_done=1;%si ya fue visitado no lo metemos a la lista
                            cond=1;
                            tablero(next_move(1),next_move(2))=0;
                            continue;
                        end
                    end
                    if move_done==0%si no es visiado lo pintamos y agragamos a la lista                            
                        index_i=next_move(1);index_j=next_move(2);
                        posx=(-reinas+index_i)-.5;
                        posy=-index_j+.5;
                        text(posx,posy,'R');
                        fou=1;
                        pause(.3);%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        arbol(index_raiz)={move};%lo ponemos al tope de la lista
                        visitado(index_vis)={move};%lo ponemos como edo visitado
                        index_raiz=index_raiz+1;%incrementamos contador
                        index_vis=index_vis+1;
                        break;
                    end
                    
                end
            end        
            if fou==1
                break;
            end
        end

        if cond==1%si no hay mas movimientos
            index_raiz=index_raiz-1;        
            arbol{index_raiz}={};%borramos el ultimo de la lsita
            tablero=cell2mat(arbol(index_raiz-1));%tomamos el de la pila de la lista como estado inicial
            [ix iy]=find(9==tablero);%pintamos en el tablero
            queens_in=[ix iy];
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            hold off

            x=-reinas:1:0;
            yx=-reinas:1:0;
            b=-reinas:0;
            c=-reinas:0;
            m=0;
            for i=1:length(b)%pintamos el tablero
                y=m*x+b(i);
                plot(x,y);
                x1=m*yx+c(i);
                plot(x1,yx);
                hold on
            end

            %%%%%pintamos en el tablero las reinas
            for n=1:size(queens_in,1)
                index_i=queens_in(n,1);index_j=queens_in(n,2);
                posx=(-reinas+index_i)-.5;
                posy=-index_j+.5;
                text(posx,posy,'R');                
            end
            pause(.5);%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%
            %disp('No hay posibles. Retroceso')
        else
            tablero=move;%nueva jugada
            solu=find(9==tablero);%verificar si ya llegamos a la solucion
            if size(solu,1)==reinas%solucion termiar
                flag=2;
                disp('solucion encontrada');
            end
        end
    end
end%continuar
toc;
