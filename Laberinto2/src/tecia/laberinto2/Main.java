package tecia.laberinto2;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import tecia.laberinto2.view.ConfigureModalController;
import tecia.laberinto2.view.RootLayoutController;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	private Stage rootStage;
	private BorderPane rootPane;
	private RootLayoutController controlRoot;

	public Main() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void start(Stage primaryStage) {
		this.rootStage = primaryStage;
		this.rootStage.setTitle("Laberinto");
		this.rootStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			public void handle(WindowEvent we){
				try {
					Alert close = new Alert(Alert.AlertType.CONFIRMATION);
					close.setHeaderText("Saliendo...");
					close.setContentText("�Desea Cerrar la Aplicaci�n?");
					
					close.showAndWait().ifPresent(response -> {
						if ( response == ButtonType.OK ) {
							Platform.exit();
						} else {
							we.consume();
						}
					});
				} catch (Exception e) {
					System.err.println(e);
				}
			}
		});
		
		initRootLayout();
		initConfigurePaneModal();
	}
	
	private void initConfigurePaneModal() {
		try {
			/* Load View */
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/ConfigureModalView.fxml"));
			AnchorPane anchorPane = (AnchorPane) loader.load();
			
			/* Prepare Modal */
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Configurar Aplicaci�n");
			dialogStage.initModality(Modality.APPLICATION_MODAL);
			dialogStage.initOwner(rootStage);
			dialogStage.setScene(new Scene(anchorPane));
			
			/* Load Controller */
			ConfigureModalController control = loader.getController();
			control.setDialogStage(dialogStage);
			control.setAppMain(this);
			
			dialogStage.showAndWait();
			
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		
	}

	private void initRootLayout() {
		try {
			/* Load View */
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/RootLayoutView.fxml"));
			rootPane = (BorderPane) loader.load();
			
			Scene rootScene = new Scene(rootPane);
			rootStage.setScene(rootScene);
			
			/* Load Controller */
			controlRoot = loader.getController();
			controlRoot.setAppMain(this);
			
			rootStage.show();
			
		} catch (IOException e) {
			System.err.println(e);
		}		
	}

	public RootLayoutController getControlRoot() {
		return this.controlRoot;
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
