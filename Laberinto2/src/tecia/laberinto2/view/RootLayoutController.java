package tecia.laberinto2.view;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.paint.Color;
import tecia.laberinto2.Main;

public class RootLayoutController {
	private Main appMain;
	
	@FXML MenuItem mnItmRestart;
	@FXML MenuItem mnItmClose;
	@FXML MenuItem mnItmToggleConsole;
	@FXML MenuItem mnItemAbout;
	@FXML TextArea txtConsole;
	@FXML Canvas cnvsLabyrinth;
	
	GraphicsContext graphContext;
	
	public RootLayoutController() {
		// TODO Auto-generated constructor stub
	}
	
	@FXML
	private void initialize() {
		graphContext = cnvsLabyrinth.getGraphicsContext2D();
		graphContext.setFill(Color.WHITE);
		graphContext.setStroke(Color.WHITE);
		
		graphContext.fillRect(0, 0, cnvsLabyrinth.getWidth(), cnvsLabyrinth.getHeight());
	}

	/**
	 * @return the appMain
	 */
	public Main getAppMain() {
		return appMain;
	}

	/**
	 * @param appMain the appMain to set
	 */
	public void setAppMain(Main appMain) {
		this.appMain = appMain;
	}
}
