package tecia.laberinto2.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.stage.Stage;
import tecia.laberinto2.Main;

public class ConfigureModalController {
	private Main appMain;
	private Stage dialogStage;
	
	@FXML private TitledPane manualConfig;
	@FXML private TitledPane autoConfig;
	@FXML private TextField txtWidthManual;
	@FXML private TextField txtHeightManual;
	@FXML private Label lblFileName;
	@FXML private Button btnFileSearch;
	@FXML private Button btnAccept;
	
	public ConfigureModalController() {
		// TODO Auto-generated constructor stub
	}
	
	@FXML
	private void initialize() {
		
	}

	public void handleChooseFile(ActionEvent event) {
		appMain.getControlRoot().txtConsole.setText(event.toString());
	}
	/**
	 * @return the appMain
	 */
	public Main getAppMain() {
		return appMain;
	}

	/**
	 * @param appMain the appMain to set
	 */
	public void setAppMain(Main appMain) {
		this.appMain = appMain;
	}

	/**
	 * @return the dialogStage
	 */
	public Stage getDialogStage() {
		return dialogStage;
	}

	/**
	 * @param dialogStage the dialogStage to set
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}
}
