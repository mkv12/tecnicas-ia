package aestrella;

import java.util.*;
import java.io.*;

/**
 * Definimos el estado de las celdas como: 0 = Celda normal 1 = Celda de salida
 * 2 = Celda de llegada 3 = Celda imposible 4 = Celda dificil 5 = Celda muy
 * dificil
 */
@SuppressWarnings("serial")
public class Celda implements Comparable<Object>, Cloneable, Serializable {
	int fila;
	int columna;
	int estado;
	int g;
	int h;
	Vector<Celda> hijos;
	int f_padre;
	int c_padre;
	boolean visible;

	public Celda(int f, int c) {
		fila = f;
		columna = c;
		estado = 0;
		g = 0;
		h = 0;
		hijos = new Vector<Celda>(9);
		f_padre = 0;
		c_padre = 0;
		visible = false;
	}

	public Celda(Celda c) {
		fila = c.getFila();
		columna = c.getColumna();
		estado = c.getEstado();
		g = c.g();
		h = c.h();
		visible = false;
		hijos = new Vector<Celda>(c.hijos);
		f_padre = c.getPadre().getFila();
		c_padre = c.getPadre().getColumna();
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int e) {
		estado = e;
	}

	public int compareTo(Object o) {
		return this.f() - ((Celda) o).f();
	}

	public int getFila() {
		return fila;
	}

	public int getColumna() {
		return columna;
	}

	public void setFC(int f, int c) {
		fila = f;
		columna = c;
	}

	public int coste() {
		int c = 0;
		switch (estado) {
		case 0:
			c = 1;
			break;
		case 4:
			c = 2;
			break;
		case 5:
			c = 3;
			break;
		default:
			c = 1;
		}
		return c;
	}

	public int g() {
		return g;
	}

	public int h() {
		return h;
	}

	public int f() {
		return g + h;
	}

	public void setG(int _g) {
		g = _g;
	}

	public void setH(int _h) {
		h = _h;
	}

	public void setPadre(int fp, int cp) {
		f_padre = fp;
		c_padre = cp;
	}

	public Celda getPadre() {
		return new Celda(f_padre, c_padre);
	}

	public void anyadeHijo(Celda c) {
		hijos.addElement(new Celda(c));
	}

	@SuppressWarnings("unchecked")
	public Vector<Celda> devuelveHijos() {
		return (Vector<Celda>) (hijos.clone());
	}

	public void hazVisible(int _g) {
		visible = true;
		g = _g;
	}

	public void hazInvisible() {
		visible = false;
	}

	public boolean visible() {
		return visible;
	}

	public boolean igualCoordenadas(Celda c) {
		return ((fila == c.getFila()) && (columna == c.getColumna()));
	}
}