package aestrella;

import javax.swing.*;
import java.awt.*;
import java.util.*;

@SuppressWarnings("serial")
public class RepresentaMapa extends JPanel {
	int filas;
	int columnas;
	int tamanyo;
	MapaCeldas mapa;
	Vector<Celda> vectorSolucion;

	public RepresentaMapa(int f, int c, int tam, MapaCeldas m) {
		super();
		filas = f;
		columnas = c;
		tamanyo = tam;
		mapa = m;
	}

	public void setMapa(MapaCeldas m) {
		mapa = m;
	}

	@Override
	public void paint(Graphics g) {
		int e;
		for (int fi = 0; fi < filas; fi++)
			for (int ci = 0; ci < columnas; ci++) {
				e = mapa.getEstado(fi, ci);
				switch (e) {
				case 1:
					g.setColor(Color.blue);
					break;
				case 2:
					g.setColor(Color.red);
					break;
				case 0:
					g.setColor(Color.yellow);
					break;
				case 3:
					g.setColor(Color.black);
					break;
				case 4:
					g.setColor(Color.gray);
					break;
				case 5:
					g.setColor(Color.darkGray);
					break;
				}
				g.fill3DRect(ci * tamanyo, fi * tamanyo, tamanyo - 1, tamanyo - 1, true);
				if (mapa.celdaVisible(fi, ci)) {
					g.setColor(Color.black);
					Integer txt = new Integer(mapa.getG(fi, ci));
					g.drawString(txt.toString(), ci * tamanyo, fi * tamanyo + 14);
				}
			}
		if (vectorSolucion != null) {
			for (int i = 1; i < vectorSolucion.size() - 1; i++) {
				g.setColor(Color.green);
				int fi = ((Celda) vectorSolucion.get(i)).getFila();
				int ci = ((Celda) vectorSolucion.get(i)).getColumna();
				g.fill3DRect(ci * tamanyo, fi * tamanyo, tamanyo - 1, tamanyo - 1, true);
				if (mapa.celdaVisible(fi, ci)) {
					g.setColor(Color.black);
					Integer txt = new Integer(mapa.getG(fi, ci));
					g.drawString(txt.toString(), ci * tamanyo, fi * tamanyo + 14);
				}

			}
			vectorSolucion = null;
			mapa.invisible();
		}

	}

	public void pintarSolucion(Vector<Celda> v) {
		vectorSolucion = new Vector<Celda>(v);
	}

}