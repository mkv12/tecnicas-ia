package aestrella;

import java.util.*;
import javax.swing.*;

public class AEstrella {
	ListaOrdenada abiertos;
	ListaOrdenada cerrados;

	Celda salida;
	Celda llegada;
	Celda mejorNodo;

	MapaCeldas mapa;
	JPanel padre;

	public AEstrella(MapaCeldas m, JPanel p) {
		mapa = m;
		salida = mapa.getSalida();
		llegada = mapa.getLlegada();
		abiertos = new ListaOrdenada();
		cerrados = new ListaOrdenada();
		padre = p;
	}

	/**
	 * Se devuelve el vector de celdas solucion
	 */
	public Vector<Celda> encuentraCamino() {
		Vector<Celda> sucesores;

		salida.setH(estimacion(salida));
		abiertos.anyade(salida);

		boolean sol_encontrada = false;

		while ((!sol_encontrada) && (!abiertos.vacia())) {
			mejorNodo = new Celda(abiertos.eliminaMinimo());

			if (mejorNodo.igualCoordenadas(llegada))
				sol_encontrada = true;
			else {
				sucesores = generaSucesoresDirigidos(mejorNodo);

				for (int i = 0; i < sucesores.size(); i++) {
					Celda suc = (Celda) sucesores.get(i);
					if (suc != null) {

						if (suc.getEstado() != 3) {
							tratarSucesor(suc);
							mejorNodo.anyadeHijo(suc);
						}
					}
				}
				cerrados.anyade(mejorNodo);
				mapa.hazCeldaVisible(mejorNodo.getFila(), mejorNodo.getColumna(), mejorNodo.g());
				padre.repaint();

				// System.out.println("Casilla pintada"+mejorNodo.g());
			}
		}
		// calculamos la solucion
		Celda aux = new Celda(mejorNodo);
		Celda paux;

		Vector<Celda> v = new Vector<Celda>(20);
		v.add(llegada);
		while (!aux.igualCoordenadas(salida)) {

			v.add(aux);

			// try{ System.in.read();} catch (Exception e){}
			paux = aux.getPadre();
			int pos = cerrados.posicion(paux);
			// System.out.println(pos);
			aux = cerrados.getElem(pos);

		}
		v.remove(0);
		v.add(salida);
		return v;

	}

	protected void tratarSucesor(Celda s) {
		s.setPadre(mejorNodo.getFila(), mejorNodo.getColumna());
		// Dependiendo del tipo de celda se sumara un peso
		s.setG(mejorNodo.g() + mapa.costeCelda(s.getFila(), s.getColumna()));

		int pos;
		Celda viejo;

		if ((pos = abiertos.posicion(s)) != -1) {
			viejo = abiertos.getElem(pos);
			abiertos.eliminaCelda(viejo);

			if (s.g() < viejo.g()) {
				viejo.setPadre(mejorNodo.getFila(), mejorNodo.getColumna());
				viejo.setG(s.g());
			}
			abiertos.anyade(viejo);
		} else if ((pos = cerrados.posicion(s)) != -1) {
			viejo = cerrados.getElem(pos);
			cerrados.eliminaCelda(viejo);

			if (s.g() < viejo.g()) {
				viejo.setPadre(mejorNodo.getFila(), mejorNodo.getColumna());
				viejo.setG(s.g());
				propagaG(viejo);
			}
			cerrados.anyade(viejo);

		} else {
			s.setH(estimacion(s));
			abiertos.anyade(s);
		}

	}

	private void propagaG(Celda c) {
		int pos;
		Celda aux;
		Vector<?> sucs = c.devuelveHijos();
		Celda s;
		for (int i = 0; i < sucs.size(); i++) {
			s = new Celda((Celda) sucs.get(i));
			if ((pos = abiertos.posicion(s)) != -1) {
				aux = abiertos.getElem(pos);
				abiertos.eliminaCelda(aux);
				// g() se modifica dependiendo del tipo de casilla
				aux.setG(c.g() + 1);
				aux.setG(c.g() + mapa.costeCelda(aux.getFila(), aux.getColumna()));
				abiertos.anyade(aux);
			}

			if ((pos = cerrados.posicion(s)) != -1) {
				aux = cerrados.getElem(pos);
				cerrados.eliminaCelda(aux);
				aux.setG(c.g() + mapa.costeCelda(aux.getFila(), aux.getColumna()));
				cerrados.anyade(aux);
				propagaG(aux);
			}
		}
	}

	public int estimacion(Celda c) {
		return Math.max(Math.abs(c.getFila() - llegada.getFila()), Math.abs(c.getColumna() - llegada.getColumna()));
	}

	public Vector<Celda> generaSucesores(Celda c) {
		int col = c.getColumna();
		int fil = c.getFila();
		Vector<Celda> adyacentes = new Vector<Celda>(9);
		// Nos recorremos todas las adyacentes
		for (int i = col - 1; i <= col + 1; i++)
			for (int j = fil - 1; j <= fil + 1; j++) {
				// Con esta condicion nos quitamos las casillas que estan fuera
				// de la matriz y la misma casilla en [i][j].
				if ((j >= 0) && (j < mapa.getFilasMapa()) && (i >= 0) && (i < mapa.getColumnasMapa())
						&& ((i != col) || (j != fil)))
					// hacemos una copia de cada celda, ya que indicamos los
					// sucesores segun sus coordenadas
					adyacentes.add(mapa.getCelda(j, i));
			}

		return adyacentes;
	}

	public Vector<Celda> generaSucesoresDirigidos(Celda c) {
		int col = c.getColumna();
		int fil = c.getFila();
		Vector<Celda> adyacentes = new Vector<Celda>(9);
		int lleg_f = llegada.getFila();
		int lleg_c = llegada.getColumna();
		int i = 0; // Columnas
		int j = 0; // Filas
		int[] v = new int[8];
		// direcciones: 1 2 3
		// 4 5
		// 6 7 8
		int[] dcha = { 5, 8, 3, 7, 2, 1, 6, 4 };
		int[] izqda = { 4, 6, 1, 2, 7, 3, 8, 5 };
		int[] arriba = { 2, 3, 1, 5, 4, 8, 6, 7 };
		int[] abajo = { 7, 6, 8, 4, 5, 1, 3, 2 };
		int[] abajoDcha = { 8, 5, 7, 3, 6, 2, 4, 1 };
		int[] arribaIzqda = { 1, 4, 2, 6, 3, 7, 5, 8 };
		int[] abajoIzqda = { 6, 7, 4, 8, 1, 5, 2, 3 };
		int[] arribaDcha = { 3, 2, 5, 1, 8, 4, 7, 6 };

		if (fil == lleg_f) // Horizontal
		{
			if (col < lleg_c) // hacia la dcha
				for (int a = 0; a < 8; a++)
					v[a] = dcha[a];
			if (col > lleg_c) // hacia la izqda
				for (int a = 0; a < 8; a++)
					v[a] = izqda[a];
		}
		if (col == lleg_c) // Vertical
		{
			if (fil < lleg_f) // hacia abajo
				for (int a = 0; a < 8; a++)
					v[a] = abajo[a];
			if (fil > lleg_f) // hacia arriba
				for (int a = 0; a < 8; a++)
					v[a] = arriba[a];
		}
		if ((fil < lleg_f) && (col < lleg_c)) // abajo-dcha
			for (int a = 0; a < 8; a++)
				v[a] = abajoDcha[a];

		if ((fil > lleg_f) && (col > lleg_c)) // arriba-izqda
			for (int a = 0; a < 8; a++)
				v[a] = arribaIzqda[a];
		if ((fil < lleg_f) && (col > lleg_c)) // abajo-izqda
			for (int a = 0; a < 8; a++)
				v[a] = abajoIzqda[a];
		if ((fil > lleg_f) && (col < lleg_c)) // arriba-dcha
			for (int a = 0; a < 8; a++)
				v[a] = arribaDcha[a];

		for (int k = 0; k < 8; k++) {
			switch (v[k]) {
			case 1:
				i = col - 1;
				j = fil - 1;
				break;
			case 2:
				i = col;
				j = fil - 1;
				break;
			case 3:
				i = col + 1;
				j = fil - 1;
				break;
			case 4:
				i = col - 1;
				j = fil;
				break;
			case 5:
				i = col + 1;
				j = fil;
				break;
			case 6:
				i = col - 1;
				j = fil + 1;
				break;
			case 7:
				i = col;
				j = fil + 1;
				break;
			case 8:
				i = col + 1;
				j = fil + 1;
				break;
			}

			// Con esta condicion nos quitamos las casillas que estan fuera de
			// la matriz y la misma casilla en [i][j].
			if ((j >= 0) && (j < mapa.getFilasMapa()) && (i >= 0) && (i < mapa.getColumnasMapa())
					&& ((i != col) || (j != fil)))
				adyacentes.add(mapa.getCelda(j, i));

		}

		return adyacentes;
	}

}