package aestrella;

import java.util.*;

public class ListaOrdenada {
	protected Vector<Celda> v;

	public ListaOrdenada() {
		v = new Vector<Celda>(200);
	}

	public void anyade(Celda o) {
		v.addElement(new Celda(o));
		Collections.sort(v);
	}

	public Celda getElem(int p) {
		return (Celda) v.get(p);
	}

	public Celda minimo() {
		return (Celda) v.elementAt(0);
	}

	public Celda eliminaMinimo() {
		return (Celda) v.remove(0);
	}

	/**
	 * Elimina la celda c de la lista ordenada
	 */
	public void eliminaCelda(Celda c) {
		int indice = -1;
		for (int i = 0; i < v.size(); i++) {
			if ((c.getFila() == ((Celda) v.elementAt(i)).getFila())
					&& (c.getColumna() == ((Celda) v.elementAt(i)).getColumna()))
				indice = i;

		}
		if (indice != -1)
			v.remove(indice);
	}

	public boolean vacia() {
		return v.size() == 0;
	}

	/**
	 * Nos devuelve la posicion del la celda c en el array Si no esta devuelve
	 * -1
	 */
	public int posicion(Celda c) {
		int indice = -1;
		for (int i = 0; i < v.size(); i++) {
			if ((c.getFila() == ((Celda) v.elementAt(i)).getFila())
					&& (c.getColumna() == ((Celda) v.elementAt(i)).getColumna()))
				indice = i;

		}
		return indice;
	}
}