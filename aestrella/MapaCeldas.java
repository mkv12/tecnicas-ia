package aestrella;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MapaCeldas implements Serializable{
	Celda[][] matriz;
	int filas;
	int columnas;
	Celda inicio;
	Celda fin;

	public MapaCeldas(int f, int c) {
		filas = f;
		columnas = c;
		matriz = new Celda[f][c];
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				matriz[i][j] = new Celda(i, j);
			}
		}
		inicio = new Celda(0, 0);
		matriz[0][0].setEstado(1);
		fin = new Celda(f - 1, c - 1);
		matriz[f - 1][c - 1].setEstado(2);
	}

	public void inicializa() {
		for (int i = 0; i < filas; i++)
			for (int j = 0; j < columnas; j++) {
				matriz[i][j].setEstado(0);
				matriz[i][j].hazInvisible();
			}
		matriz[inicio.getFila()][inicio.getColumna()].setEstado(1);
		// matriz[0][0].setEstado(1);
		matriz[fin.getFila()][fin.getColumna()].setEstado(2);
		// matriz[filas-1][columnas-1].setEstado(2);
	}

	public void invisible() {
		for (int i = 0; i < filas; i++)
			for (int j = 0; j < columnas; j++)
				matriz[i][j].hazInvisible();
	}

	public int getEstado(int i, int j) {
		return matriz[i][j].getEstado();
	}

	public void setEstado(int i, int j, int e) {
		if (e == 1) {
			matriz[inicio.getFila()][inicio.getColumna()].setEstado(0);
			inicio.setFC(i, j);
		}
		if (e == 2) {
			matriz[fin.getFila()][fin.getColumna()].setEstado(0);
			fin.setFC(i, j);
		}
		matriz[i][j].setEstado(e);
	}

	public Celda getSalida() {
		return inicio;
	}

	public Celda getLlegada() {
		return fin;
	}

	public int costeCelda(int f, int c) {
		return matriz[f][c].coste();
	}

	public Celda getCelda(int f, int c) {
		return new Celda(matriz[f][c]);
	}

	public void hazCeldaVisible(int f, int c, int _g) {
		matriz[f][c].hazVisible(_g);
	}

	public int getG(int f, int c) {
		return matriz[f][c].g();
	}

	public boolean celdaVisible(int f, int c) {
		return matriz[f][c].visible();
	}

	public int getFilasMapa() {
		return filas;
	}

	public int getColumnasMapa() {
		return columnas;
	}

	// Como en los applets no podemos cargar ficheros, tenemos que indicar los
	// mapas aqui.
	// Estos datos se obtienen al guardar un mapa
	public void preestablece(int m) {
		inicializa();
		
		if (m == 1) {
			setEstado(0, 0, 1);
			setEstado(2, 13, 2);
			int imf[] = { 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5,
					6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 9, 9, 9, 10, 10, 10, 10, 10, 10,
					10, 11, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 13, 13, 13 };
			int imc[] = { 1, 2, 2, 3, 4, 7, 2, 3, 4, 5, 7, 2, 3, 4, 7, 8, 2, 3, 4, 7, 8, 9, 2, 3, 4, 7, 8, 9, 10, 11,
					12, 2, 3, 4, 5, 9, 10, 11, 12, 2, 3, 4, 5, 6, 10, 11, 12, 3, 4, 5, 6, 11, 12, 3, 4, 5, 2, 3, 4, 5,
					8, 9, 10, 3, 4, 5, 8, 9, 10, 11, 8, 9, 10, 11, 12, 10, 11, 12 };
			int df[] = { 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 8, 8, 9, 9, 10, 11, 12, 12, 13, 13, 13, 13, 14, 14, 14,
					14, 14 };
			int dc[] = { 9, 10, 11, 10, 11, 12, 10, 11, 12, 11, 12, 13, 14, 0, 1, 0, 1, 0, 0, 1, 2, 2, 3, 4, 5, 3, 4, 5,
					6, 7 };
			int mdf[] = { 0, 0, 0, 1, 1, 2, 3, 12, 13, 13, 14, 14, 14 };
			int mdc[] = { 12, 13, 14, 13, 14, 14, 14, 0, 0, 1, 0, 1, 2 };
			for (int i = 0; i < imf.length; i++) {
				setEstado(imf[i], imc[i], 3);
			}
			for (int i = 0; i < df.length; i++) {
				setEstado(df[i], dc[i], 4);
			}
			for (int i = 0; i < mdf.length; i++) {
				setEstado(mdf[i], mdc[i], 5);
			}
		} else if (m == 2) {
			setEstado(14, 14, 1);
			setEstado(3, 0, 2);
			int imf[] = { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
					3, 3, 3, 3, 3, 3, 4, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8,
					8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 11, 11, 11, 11, 11, 11, 11, 12, 12,
					12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14 };
			int imc[] = { 1, 2, 3, 4, 5, 6, 12, 13, 0, 1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 1, 2, 3, 4, 5, 6, 7, 11, 12,
					13, 14, 2, 3, 4, 5, 6, 12, 5, 12, 13, 5, 6, 7, 8, 11, 12, 13, 14, 2, 3, 4, 5, 6, 7, 8, 10, 11, 13,
					14, 1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 1, 2, 3, 4, 5, 6, 7, 12, 13, 14, 12, 13, 14, 2, 3, 4, 5,
					6, 13, 14, 2, 3, 4, 5, 6, 7, 2, 3, 4, 5, 6, 7, 8, 9, 10, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
			int df[] = {};
			int dc[] = {};
			int mdf[] = {};
			int mdc[] = {};
			for (int i = 0; i < imf.length; i++) {
				setEstado(imf[i], imc[i], 3);
			}
			for (int i = 0; i < df.length; i++) {
				setEstado(df[i], dc[i], 4);
			}
			for (int i = 0; i < mdf.length; i++) {
				setEstado(mdf[i], mdc[i], 5);
			}

		} else if (m == 3) {
			setEstado(13, 1, 1);
			setEstado(6, 9, 2);
			int imf[] = { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5,
					5, 6, 6, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13,
					14 };
			int imc[] = { 4, 5, 6, 7, 11, 12, 13, 3, 4, 5, 6, 7, 10, 11, 12, 13, 3, 4, 5, 6, 7, 11, 12, 3, 4, 5, 6, 7,
					2, 3, 4, 5, 3, 4, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 4, 5, 12, 13, 14, 2, 3, 4, 11, 12, 13, 14, 12 };
			int df[] = { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 3, 3, 3, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6,
					6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10,
					10, 11, 11, 11, 11, 11, 11, 11, 12, 12, 12, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14, 14 };
			int dc[] = { 0, 1, 3, 8, 9, 10, 14, 0, 9, 10, 0, 0, 1, 9, 0, 1, 9, 10, 13, 14, 0, 11, 12, 13, 14, 0, 6, 7,
					8, 11, 12, 13, 14, 0, 1, 5, 6, 7, 8, 9, 13, 14, 4, 5, 6, 10, 11, 14, 0, 5, 6, 10, 11, 13, 14, 0, 4,
					5, 6, 9, 10, 11, 13, 14, 5, 6, 7, 8, 9, 10, 14, 7, 8, 9, 0, 6, 7, 8, 9, 0, 1, 6, 7, 8, 9, 10 };
			int mdf[] = { 8, 8, 8, 9, 9, 9, 10, 10 };
			int mdc[] = { 7, 8, 9, 7, 8, 9, 7, 8 };
			for (int i = 0; i < imf.length; i++) {
				setEstado(imf[i], imc[i], 3);
			}
			for (int i = 0; i < df.length; i++) {
				setEstado(df[i], dc[i], 4);
			}
			for (int i = 0; i < mdf.length; i++) {
				setEstado(mdf[i], mdc[i], 5);
			}
		} else if (m == 4) {
			setEstado(1, 13, 1);
			setEstado(3, 9, 2);
			int imf[] = { 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5,
					5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10,
					10, 10, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 14, 14 };
			int imc[] = { 1, 2, 6, 7, 8, 9, 10, 11, 1, 2, 5, 6, 10, 11, 1, 2, 4, 5, 11, 12, 13, 3, 4, 12, 2, 3, 6, 7, 8,
					9, 10, 11, 12, 13, 2, 3, 5, 6, 7, 8, 9, 10, 2, 5, 6, 7, 1, 2, 6, 7, 10, 11, 12, 13, 1, 2, 7, 8, 9,
					10, 13, 14, 2, 3, 13, 14, 3, 4, 5, 12, 13, 1, 4, 7, 8, 9, 10, 11, 12, 1, 2, 6, 7, 10, 1, 2 };
			int df[] = {};
			int dc[] = {};
			int mdf[] = { 11, 12, 12, 13, };
			int mdc[] = { 6, 5, 6, 5, };

			for (int i = 0; i < imf.length; i++) {
				setEstado(imf[i], imc[i], 3);
			}
			for (int i = 0; i < df.length; i++) {
				setEstado(df[i], dc[i], 4);
			}
			for (int i = 0; i < mdf.length; i++) {
				setEstado(mdf[i], mdc[i], 5);
			}
		} else if (m == 5) {
			setEstado(10, 14, 1);
			setEstado(14, 1, 2);
			int imf[] = { 0, 0, 4, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 9, 9, 9, 10, 10,
					11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 13, 14, 14, 14,
					14, 14, 14, 14, 14, 14, 14 };
			int imc[] = { 0, 10, 14, 3, 4, 13, 14, 2, 3, 4, 5, 12, 13, 14, 1, 2, 3, 4, 13, 14, 0, 1, 2, 3, 14, 0, 1, 2,
					1, 10, 8, 9, 10, 11, 13, 7, 8, 9, 10, 11, 12, 13, 14, 6, 7, 8, 9, 10, 11, 12, 13, 14, 5, 6, 7, 8, 9,
					10, 11, 12, 13, 14 };
			int df[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3,
					3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 8, 8,
					8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11,
					11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14 };
			int dc[] = { 1, 2, 6, 7, 8, 9, 11, 12, 13, 0, 1, 7, 8, 9, 10, 11, 12, 0, 3, 4, 8, 9, 10, 11, 14, 2, 3, 4, 5,
					9, 10, 13, 14, 1, 2, 3, 4, 5, 6, 9, 12, 13, 0, 1, 2, 5, 6, 7, 11, 12, 0, 1, 6, 7, 10, 11, 0, 5, 6,
					9, 10, 11, 12, 4, 5, 8, 9, 10, 11, 12, 13, 3, 4, 7, 8, 9, 10, 11, 12, 13, 14, 0, 2, 3, 6, 7, 8, 9,
					11, 12, 13, 0, 1, 2, 5, 6, 7, 12, 14, 0, 1, 4, 5, 6, 0, 3, 4, 5, 2, 3, 4 };
			int mdf[] = {};
			int mdc[] = {};

			for (int i = 0; i < imf.length; i++) {
				setEstado(imf[i], imc[i], 3);
			}
			for (int i = 0; i < df.length; i++) {
				setEstado(df[i], dc[i], 4);
			}
			for (int i = 0; i < mdf.length; i++) {
				setEstado(mdf[i], mdc[i], 5);
			}
		} else if (m == 6) {
			setEstado(1, 1, 1);
			setEstado(13, 13, 2);
		}
	}

	public void pintaValores() {
		String imf = new String("int imf[]={");
		String imc = new String("int imc[]={");

		String df = new String("int df[]={");
		String dc = new String("int dc[]={");

		String mdf = new String("int mdf[]={");
		String mdc = new String("int mdc[]={");

		for (int i = 0; i < filas; i++)
			for (int j = 0; j < columnas; j++) {
				if (matriz[i][j].getEstado() == 3) {
					imf = imf + i + ",";
					imc = imc + j + ",";
				}
				if (matriz[i][j].getEstado() == 4) {
					df = df + i + ",";
					dc = dc + j + ",";
				}
				if (matriz[i][j].getEstado() == 5) {
					mdf = mdf + i + ",";
					mdc = mdc + j + ",";
				}

			}

		imf = imf + "};";
		imc = imc + "};";
		df = df + "};";
		dc = dc + "};";
		mdf = mdf + "};";
		mdc = mdc + "};";

		System.out.println("mapa.setEstado(" + inicio.getFila() + "," + inicio.getColumna() + ",1);");
		System.out.println("mapa.setEstado(" + fin.getFila() + "," + fin.getColumna() + ",2);");
		System.out.println(imf);
		System.out.println(imc);
		System.out.println(df);
		System.out.println(dc);
		System.out.println(mdf);
		System.out.println(mdc);

	}

}
