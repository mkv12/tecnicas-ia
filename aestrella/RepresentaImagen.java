package aestrella;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class RepresentaImagen extends JPanel {
	int filas;
	int columnas;
	int tamanyo;
	Image imagen;
	Image coche;
	int x_coche;
	int y_coche;

	public RepresentaImagen(int f, int c, int tam, Image i) {
		super();
		filas = f;
		columnas = c;
		tamanyo = tam;
		imagen = i;
		setPreferredSize(new Dimension(filas * tam, columnas * tam));
		coche = null;
		x_coche = 0;
		y_coche = 0;
	}

	public void paint(Graphics g) {

		g.drawImage(imagen, 0, 0, columnas * tamanyo, filas * tamanyo, this);
		if (coche != null) {
			int w = coche.getWidth(null);
			int h = coche.getHeight(null);

			g.drawImage(coche, x_coche - (w / 2), y_coche - (h / 2), w, h, this);
			coche = null;
		}
	}

	public void actualiza() {
		repaint(getBounds());
	}

	public void pintaCoche(int x, int y, Image im) {
		coche = im;
		x_coche = x;
		y_coche = y;
	}

	public void setImage(Image i) {
		imagen = i;
	}
}