package aestrella;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

@SuppressWarnings("serial")
public class appletAEstrella extends JFrame {
	public boolean modoAplicacion = false;
	JPanel panel;
	JPanel panelTitulo;
	JPanel panelOpciones;
	JPanel panelMapas;
	JPanel panelAutores;
	JLabel etiquetaTitulo;
	JLabel autor1;
	JLabel autor2;

	RepresentaMapa repMapa;
	RepresentaImagen repImg;
	Image im;
	MapaCeldas mapa;

	JComboBox<String> tipoCasilla;
	JComboBox<String> cargarMapa;
	JButton guardarMapa;
	JButton empezar;
	JButton limpiar;
	AEstrella algoritmo;
	int filas;
	int columnas;
	int tam;

	Vector<Image> coche;

	public void init() {
		filas = 15;
		columnas = 15;
		tam = 20;

		mapa = new MapaCeldas(filas, columnas);

		panel = new JPanel();
		// panel.setLayout(new GridLayout(4,1));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		etiquetaTitulo = new JLabel("Demostración de A*");
		etiquetaTitulo.setForeground(Color.red);
		etiquetaTitulo.setFont(new Font(null, Font.BOLD, 18));
		etiquetaTitulo.setVerticalAlignment(JLabel.CENTER);
		autor1 = new JLabel("Juan Ant. Recio");
		autor1.setFont(new Font(null, Font.PLAIN, 10));
		autor2 = new JLabel("Luis Dominguez");
		autor2.setFont(new Font(null, Font.PLAIN, 10));

		panelAutores = new JPanel();
		panelAutores.setLayout(new BoxLayout(panelAutores, BoxLayout.Y_AXIS));
		panelAutores.add(autor1);
		panelAutores.add(autor2);
		panelTitulo = new JPanel();
		panelTitulo.setLayout(new BoxLayout(panelTitulo, BoxLayout.X_AXIS));
		panelTitulo.add(Box.createHorizontalGlue());
		panelTitulo.add(etiquetaTitulo);
		panelTitulo.add(Box.createHorizontalGlue());
		panelTitulo.add(panelAutores);

		panelMapas = new JPanel();
		panelMapas.setLayout(new BoxLayout(panelMapas, BoxLayout.X_AXIS));

		repMapa = new RepresentaMapa(filas, columnas, tam, mapa);
		repMapa.setPreferredSize(new Dimension(filas * tam, columnas * tam));
		panelMapas.add(repMapa);
		panelMapas.add(Box.createGlue());

		cargarMapa = new JComboBox<String>();
		cargarMapa.addItem("mapa 1");
		cargarMapa.addItem("mapa 2");
		cargarMapa.addItem("mapa 3");
		cargarMapa.addItem("mapa 4");
		cargarMapa.addItem("mapa 5");
		cargarMapa.addItem("vacio");
		cargarMapa.setMaximumSize(new Dimension(80, 20));
		cargarMapa.setPreferredSize(new Dimension(100, 20));

		coche = new Vector<Image>(8);
		try {
			repImg = new RepresentaImagen(filas, columnas, tam, im);
			repImg.setPreferredSize(new Dimension(filas * tam, columnas * tam));
			panelMapas.add(repImg);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		panelOpciones = new JPanel();
		panelOpciones.setLayout(new BoxLayout(panelOpciones, BoxLayout.X_AXIS));

		JPanel subpanel1 = new JPanel();
		subpanel1.setLayout(new GridLayout(2, 1));
		empezar = new JButton("Empezar");
		empezar.setPreferredSize(new Dimension(90, 20));
		limpiar = new JButton("Limpiar");
		limpiar.setPreferredSize(new Dimension(90, 20));
		subpanel1.add(empezar);
		subpanel1.add(limpiar);

		tipoCasilla = new JComboBox<String>();
		tipoCasilla.addItem("Normal");
		tipoCasilla.addItem("Salida");
		tipoCasilla.addItem("Llegada");
		tipoCasilla.addItem("Imposible");
		tipoCasilla.addItem("Dificil");
		tipoCasilla.addItem("Muy Dificil");
		tipoCasilla.setMaximumSize(new Dimension(80, 20));
		tipoCasilla.setPreferredSize(new Dimension(100, 20));

		guardarMapa = new JButton("Guardar");

		// panelOpciones.add(Box.createHorizontalGlue());
		panelOpciones.add(tipoCasilla);
		panelOpciones.add(Box.createHorizontalGlue());

		panelOpciones.add(guardarMapa);
		panelOpciones.add(Box.createHorizontalGlue());

		panelOpciones.add(subpanel1);
		panelOpciones.add(Box.createHorizontalGlue());
		panelOpciones.add(cargarMapa);

		panel.add(panelTitulo);
		panel.add(panelMapas);
		panel.add(panelOpciones);
		getContentPane().add(panel);

		repMapa.addMouseListener(new OyenteCeldas());
		limpiar.addMouseListener(new OyenteLimpiar());
		empezar.addMouseListener(new OyenteEmpezar());
		guardarMapa.addMouseListener(new OyenteGuardar());
		cargarMapa.addActionListener(new OyenteCargar());

		setVisible(true);

		cargar();

		repaint();

		algoritmo = new AEstrella(mapa, panelMapas);
	}

	class OyenteCeldas extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			if ((e.getY() < tam * filas) && (e.getX() < tam * columnas)) {
				int tipo = tipoCasilla.getSelectedIndex();
				mapa.setEstado(e.getY() / tam, e.getX() / tam, tipo);
				panelMapas.repaint();
			}
		}
	}

	class OyenteLimpiar extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			mapa.inicializa();
			panelMapas.repaint();
		}
	}

	class OyenteGuardar extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			int indx = cargarMapa.getSelectedIndex() + 1;
			System.out.println("Guardando: mapas" + File.separator + "campo0" + indx + ".map");
			try {
				ObjectOutputStream fichero = new ObjectOutputStream(
						new FileOutputStream("mapas" + File.separator + "campo0" + indx + ".map"));
				fichero.writeObject(mapa);
				fichero.close();
			} catch (Exception x) {
				System.err.println("ERROR: Imposible guardar archivo");
				System.err.println(x);
			}
			mapa.pintaValores();

		}
	}

	class OyenteEmpezar extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			panelMapas.repaint();
			algoritmo = new AEstrella(mapa, panelMapas);
			Vector<Celda> solucion;
			// Hallamos el camino solucion y lo pintamos en el mapa
			solucion = algoritmo.encuentraCamino();
			repMapa.pintarSolucion(solucion);
			Animacion anim = new Animacion(solucion, repImg, coche, tam);
			anim.start();
		}
	}

	class OyenteCargar implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			cargar();
		}
	}

	public void cargar() {
		int indx = cargarMapa.getSelectedIndex() + 1;

		try {
			System.out.println("Cargando: " + "mapas" + File.separator + "campo0" + indx + ".gif");
			ImageIcon t = new ImageIcon("imagenes" + File.separator + "campo0" + indx + ".gif");
			im = t.getImage();

			System.out.println("Cargando: " + "mapas" + File.separator + "campo0" + indx + ".map");
			ObjectInputStream fichero = new ObjectInputStream(
					new FileInputStream("mapas" + File.separator + "campo0" + indx + ".map"));
			mapa = (MapaCeldas) fichero.readObject();
			repMapa.setMapa(mapa);
			fichero.close();
			repImg.setImage(im);
			panelMapas.repaint();
		} catch (Exception x) {
			System.err.println("ERROR de I/0");
			System.err.println(x.getMessage());
			mapa.preestablece(indx);
			panelMapas.repaint();
		}

	}

	// Main entry point when running standalone
	public static void main(String[] args) {
		appletAEstrella applet = new appletAEstrella();
		applet.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		applet.setSize(620, 420);
		applet.setTitle("Applet Frame");
		applet.init();

		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		applet.setLocation((d.width - applet.getSize().width) / 2, (d.height - applet.getSize().height) / 2);
		applet.setVisible(true);
	}

}