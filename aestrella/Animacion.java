package aestrella;

import javax.swing.*;
import java.awt.*;
import java.util.*;

public class Animacion extends Thread {
	int tam;
	Vector<Celda> pasos;
	ImageIcon[] coche;
	RepresentaImagen repImag;

	public Animacion(Vector<Celda> v, RepresentaImagen ri, Vector<Image> c, int t) {
		pasos = new Vector<Celda>(v);
		repImag = ri;
		coche = new ImageIcon[8];
		for (int i = 0; i < 8; i++)
			coche[i] = new ImageIcon((Image) c.elementAt(i));
		// { System.out.println("Null"); try{ System.in.read();} catch
		// (Exception e){} }
		tam = t;
	}

	public void run() {
		Celda c;
		Celda anterior = (Celda) pasos.elementAt(pasos.size() - 1);
		int dir = 0;
		int offx = 0;
		int offy = 0;

		for (int i = pasos.size() - 2; i >= 0; i--) {
			c = (Celda) pasos.elementAt(i);

			if ((c.getFila() == anterior.getFila()) && (c.getColumna() < anterior.getColumna())) {
				dir = 0;
				offx = tam;
				offy = tam / 2;
			} else if ((c.getFila() > anterior.getFila()) && (c.getColumna() < anterior.getColumna())) {
				dir = 1;
				offx = tam;
				offy = 0;
			} else if ((c.getFila() > anterior.getFila()) && (c.getColumna() == anterior.getColumna())) {
				dir = 2;
				offx = tam / 2;
				offy = 0;
			} else if ((c.getFila() > anterior.getFila()) && (c.getColumna() > anterior.getColumna())) {
				dir = 3;
				offx = 0;
				offy = 0;
			} else if ((c.getFila() == anterior.getFila()) && (c.getColumna() > anterior.getColumna())) {
				dir = 4;
				offx = 0;
				offy = tam / 2;
			} else if ((c.getFila() < anterior.getFila()) && (c.getColumna() > anterior.getColumna())) {
				dir = 5;
				offx = 0;
				offy = tam;
			} else if ((c.getFila() < anterior.getFila()) && (c.getColumna() == anterior.getColumna())) {
				dir = 6;
				offx = tam / 2;
				offy = tam;
			} else if ((c.getFila() < anterior.getFila()) && (c.getColumna() < anterior.getColumna())) {
				dir = 7;
				offx = tam;
				offy = tam;
			}
			anterior = new Celda(c);

			repImag.pintaCoche(c.getColumna() * tam + offx, c.getFila() * tam + offy, coche[dir].getImage());
			repImag.repaint();
			try {
				sleep(150);
			} catch (Exception e) {
			}
			repImag.pintaCoche(c.getColumna() * tam + (tam / 2), c.getFila() * tam + (tam / 2), coche[dir].getImage());
			repImag.repaint();
			try {
				sleep(150);
			} catch (Exception e) {
			}

		}
	}

}