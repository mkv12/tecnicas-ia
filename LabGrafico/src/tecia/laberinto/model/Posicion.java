package tecia.laberinto.model;

import java.util.ArrayList;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Posicion {
	private IntegerProperty posX;
	private IntegerProperty posY;
	private ArrayList<Posicion> children;
	private StringProperty posString;
	
	/**
	 * Default Constructor
	 * */
	public Posicion() {
		this(0,0);
	}
	
	/**
	 * Constructor
	 * @param px 
	 * */
	public Posicion(int px, int py) {
		if( px < 0 || py < 0)
			return;
		
		this.posX = new SimpleIntegerProperty(px);
		this.posY = new SimpleIntegerProperty(py);
		this.children = new ArrayList<>(0);
		this.posString = new SimpleStringProperty();
		this.buildPosString();
	}
	
	/**
	 * @return int posX value
	 */
	public int getPosX() {
		return this.posX.get();
	}
	
	/**
	 * @return the posX
	 */
	public IntegerProperty getPosXProperty() {
		return this.posX;
	}
	
	/**
	 * @param _px the posX to set
	 */
	public void setPosx(int _px) {
		this.posX.set(_px);
	}
	
	/**
	 * @return int posY value
	 */
	public int getPosY() {
		return this.posY.get();
	}
	
	/**
	 * @return the posY
	 */
	public IntegerProperty getPosYProperty() {
		return posY;
	}
	
	/**
	 * @param _py the posY to set
	 */
	public void setPosy(int _py) {
		this.posY.set(_py);
	}
	
	/**
	 * @return the children value
	 */
	public ArrayList<Posicion> getChildren() {
		return this.children;
	}
	
	/**
	 * @param child the child to set
	 */
	public void setChild(ArrayList<Posicion> _children) {
		this.children = _children;
	}
	
	/**
	 * @return the Posicion representation
	 */
	public String getPosString() {
		return this.posString.get();
	}

	/**
	 * @return the posString
	 */
	public StringProperty getPosStringProperty() {
		return posString;
	}

	/**
	 * @param _posString the posString to set
	 */
	public void setPosString(String _posString) {
		this.posString.set(_posString);
	}
	
	/**
	 * Build a Posicion Representation String
	 * */
	public void buildPosString() {
		StringBuffer sf = new StringBuffer();
		
		sf.append('(')
		.append(this.getPosX())
		.append(',')
		.append(this.getPosY())
		.append(')');
		
		this.setPosString(sf.toString());
	}

	public boolean comparar(Posicion unaPosicion) {
		return this.getPosX() == unaPosicion.getPosX() && this.getPosY() == unaPosicion.getPosY();
	}
}
