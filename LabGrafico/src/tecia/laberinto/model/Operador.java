package tecia.laberinto.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Operador {
	private IntegerProperty valorOperador;
	private StringProperty nombreOperador;
	
	public Operador(int valor){
		String name = null;
		switch (valor) {
		case Laberinto.UP: 
			name = "UP";
			break;
		case Laberinto.DOWN	:
			name = "DOWN";
			break;
		case Laberinto.LEFT	:
			name = "LEFT";
			break;
		case Laberinto.RIGHT	:
			name = "RIGHT";
			break;
		default:
			break;
		}
		
		if (name == null) {
			return;
		} else {
			this.nombreOperador = new SimpleStringProperty(name);
			this.valorOperador = new SimpleIntegerProperty(valor);
		}
	}

	/**
	 * @return the valorOperador
	 */
	public int getValorOperador(){
		return this.valorOperador.get();
	}
	/**
	 * @return the valorOperador Property
	 */
	public IntegerProperty getValorOperadorProperty() {
		return valorOperador;
	}

	/**
	 * @param valorOperador the valorOperador to set
	 */
	public void setValorOperador(int vOperador) {
		this.valorOperador.set(vOperador);
	}

	/**
	 * @return the nombreOperador value
	 */
	public String getNombreOperador(){
		return this.nombreOperador.get();
	}
	
	/**
	 * @return the nombreOperador Property
	 */
	public StringProperty getNombreOperadorProperty() {
		return nombreOperador;
	}

	/**
	 * @param nombreOperador the nombreOperador to set
	 */
	public void setNombreOperador(String nOperador) {
		this.nombreOperador.set(nOperador);
	}
	
	/**
	 * @param ope Operador a copiar
	 */
	public static Operador copiaOperador(Operador ope){
		return new Operador(ope.getValorOperador());
	}
	
	@Override
	public String toString() {
		return this.getNombreOperador();
	}
}
