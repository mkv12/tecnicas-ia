package tecia.laberinto.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import tecia.laberinto.Main;
import tecia.laberinto.control.ImplementacionSolucion;
import tecia.laberinto.model.Operador;
import tecia.laberinto.model.Posicion;

public class NodeListController {
	private Main mainApp;
	
	@FXML TableView<Posicion> closedTable;
	@FXML TableView<Posicion> openedTable;
	@FXML TableView<Operador> operatorTable;
	@FXML TableColumn<Posicion, String> closedColumn;
	@FXML TableColumn<Posicion, String> openedColumn;
	@FXML TableColumn<Operador, String> operatorColumn;
	@FXML Button manualButton;
	@FXML Button automaticButton;
	@FXML Button restartButon;
	@FXML Button stopButton;
	
	public NodeListController() {
		
	}
	
	@FXML
	private void initialize() {
		openedColumn.setCellValueFactory(cellData -> cellData.getValue().getPosStringProperty());
		closedColumn.setCellValueFactory(cellData -> cellData.getValue().getPosStringProperty());
		operatorColumn.setCellValueFactory(cellData -> cellData.getValue().getNombreOperadorProperty());
	}

	/**
	 * @return the mainApp
	 */
	public Main getMainApp() {
		return mainApp;
	}

	/**
	 * @param mainApp the mainApp to set
	 */
	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
		
		openedTable.setItems(ImplementacionSolucion.opendedList);
		closedTable.setItems(ImplementacionSolucion.closedList);
		operatorTable.setItems(ImplementacionSolucion.operatorList);
	}
	
	@FXML
	public void handleManualStep() {
		if(ImplementacionSolucion.firstNode){
			ImplementacionSolucion.rellenarOperadoresBasicos();
			ImplementacionSolucion.firstNode = false;
			ImplementacionSolucion.posicionActual = ImplementacionSolucion.posicionInicial;
			
			ImplementacionSolucion.closedList.add(ImplementacionSolucion.posicionActual);
			mainApp.drawingStep(ImplementacionSolucion.posicionActual.getPosX(), ImplementacionSolucion.posicionActual.getPosY());
		}
		else if ( ImplementacionSolucion.esPosicionFinal() ) {
			mainApp.toConsole("Final!!!");
			throw new IndexOutOfBoundsException("Final Finished");
		} else {
			ImplementacionSolucion.stepAllOperators();
			if(ImplementacionSolucion.PosInicialChanged){
				mainApp.drawingStep(ImplementacionSolucion.posicionActual.getPosX(), ImplementacionSolucion.posicionActual.getPosY());
				ImplementacionSolucion.PosInicialChanged = false;
			}
		}
	}
	
	@FXML
	public void handleAutomaticStep() {
		do
			try{
				handleManualStep();
			} catch(IndexOutOfBoundsException iobex) {
				mainApp.toConsole(iobex.toString());
				break;
			} 
		while (true);
	}
	
	@FXML
	public void handleReset() {
		/*Limpiar Arrays*/
		ImplementacionSolucion.closedList.clear();
		ImplementacionSolucion.opendedList.clear();
		ImplementacionSolucion.operatorList.clear();
		
		/*Limpiar Indices*/
		ImplementacionSolucion.posicionInicial = ConfigurePaneController.getPosInit();
		ImplementacionSolucion.posicionFinal = ConfigurePaneController.getPosEnd();
		ImplementacionSolucion.posicionActual = ImplementacionSolucion.posicionInicial;
		ImplementacionSolucion.operadorActual = null;
		
		/* Limpiar Banderas */
		ImplementacionSolucion.firstNode = true;
		ImplementacionSolucion.PosInicialChanged = false;
		
		/**/
		mainApp.reiniciarLaberinto();
	}
	
	@FXML
	public void handleStopStep() {
		
	}
}
