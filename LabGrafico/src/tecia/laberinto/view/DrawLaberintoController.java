package tecia.laberinto.view;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import tecia.laberinto.Main;
import tecia.laberinto.model.Laberinto;

public class DrawLaberintoController {
	private Main mainApp;
	
	@FXML Canvas drawLab;
	GraphicsContext graphContext;
	
	private static final int ANCHO_PARED = 16;
	
	public static final int SIZE_W = Laberinto.Map.length;
	public static final int SIZE_H = Laberinto.Map[0].length;
	public DrawLaberintoController(){
		
	}
	
	@FXML
	private void initialize() {
		graphContext = this.drawLab.getGraphicsContext2D();
		
		limpiarAreaDibujo();
	}
	
	public void dibujarLaberintoInicial(){
		int size_w = Laberinto.Map.length;
		int size_h = Laberinto.Map[0].length;
		/*
		mainApp.toConsole("W: " + size_w);	
		mainApp.toConsole("H: " + size_h);
		*/
		setBorderConfig(Color.BLACK, 4.0);
		
		for (int i = 0; i < size_w; i += 2){
			for (int j = 1; j < size_h; j += 2) {
				int w = i / 2;
				int h = (j - 1) / 2;
				/*Dibujar Horizontal*/
				if(Laberinto.Map[i][j] == '#'){
					graphContext.strokeLine((h) * ANCHO_PARED , w * ANCHO_PARED, (h + 1) * ANCHO_PARED, w * ANCHO_PARED);
					/*
					System.out.println(h * ANCHO_PARED + ", "+ w * ANCHO_PARED +" -> "+ (h + 1) * ANCHO_PARED +", "+ w * ANCHO_PARED);
					*/
				} else {
					/*
					System.out.println("None");
					setBorderConfig(Color.GRAY, 1.0);
					graphContext.strokeLine((h) * ANCHO_PARED , w * ANCHO_PARED, (h + 1) * ANCHO_PARED, w * ANCHO_PARED);
					setBorderConfig(Color.BLACK, 4.0);
					*/
				}
				/*Dibujar Vertical*/
				if(Laberinto.Map[j][i] == '#'){
					graphContext.strokeLine(w * ANCHO_PARED , (h) * ANCHO_PARED, w * ANCHO_PARED, (h + 1) * ANCHO_PARED);
					/*
					System.out.println(w * ANCHO_PARED +","+ (h) * ANCHO_PARED +" -> "+ w * ANCHO_PARED + "," + (h + 1) * ANCHO_PARED);
					*/
				}else {
					/*
					System.out.println("Cerrado");
					setBorderConfig(Color.GRAY, 1.0);
					graphContext.strokeLine(w * ANCHO_PARED , (h) * ANCHO_PARED, w * ANCHO_PARED, (h + 1) * ANCHO_PARED);
					setBorderConfig(Color.BLACK, 4.0);
					*/
				}
			}
		}
		
		/*
		// Detecion de Errores
		setFillConfig(Color.gray(0, 0.5), Color.BLUEVIOLET);
		for (int i = 1; i < size_w; i+=2) {
			for (int j = 1; j < size_h; j+=2) {
				if(Laberinto.Map[i][j] == '#'){
					System.out.println("Error in: " + j + ", "+ i);
					int w = i / 2;
					int h = (j - 1) / 2;
					
					graphContext.fillRect(w * ANCHO_PARED + 1, h * ANCHO_PARED , ANCHO_PARED, ANCHO_PARED);
				}
			}
		}
		*/
	}
	
	public void limpiarAreaDibujo(){
		graphContext.setFill(Color.WHITE);
		graphContext.setStroke(Color.WHITE);
		
		graphContext.fillRect(0, 0, drawLab.getWidth(), drawLab.getHeight());
	}
	
	private void setBorderConfig(Color color, double size){
		graphContext.setStroke(color);
		graphContext.setLineWidth(size);
	}
	
	private void setFillConfig(Color colorFill, Color colorStroke) {
		graphContext.setFill(colorFill);
		graphContext.setStroke(colorStroke);
		graphContext.setLineWidth(0.5);
	}
	
	public void drawStep(int i, int j) {
		setFillConfig(Color.rgb(200, 50, 50, 0.7), Color.BLUE);
		if( i % 2 == 1 )
			i = (i - 1)/2;
		else
			i = i / 2;
		if( j % 2 == 1)
			j = (j - 1) / 2;
		else
			j = j / 2;
		mainApp.toConsole("(" + i + "," + j + ")");
		//mainApp.toConsole("Draw: (" + ((j) * ANCHO_PARED) + ", "+(i)* ANCHO_PARED + ") ->"); //, ANCHO_PARED / 2, ANCHO_PARED / 2);
		graphContext.fillRect( (j) * ANCHO_PARED + 2, (i)* ANCHO_PARED + 2, ANCHO_PARED / 3 * 2, ANCHO_PARED / 3 * 2);
	}
	
	/**
	 * @return the mainApp
	 */
	public Main getMainApp() {
		return mainApp;
	}

	/**
	 * @param mainApp the mainApp to set
	 */
	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
	}
	
	public void ReiniciarLaberinto() {
		limpiarAreaDibujo();
		dibujarLaberintoInicial();
	}
}
