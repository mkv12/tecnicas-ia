package tecia.laberinto.view;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import tecia.laberinto.Main;
import tecia.laberinto.control.ImplementacionSolucion;
import tecia.laberinto.model.Posicion;

public class ConfigurePaneController {
	private Main mainApp;
	private Stage dialogStage;
	
	@FXML TextField xiTextField;
	@FXML TextField yiTextField;
	@FXML TextField xfTextField;
	@FXML TextField yfTextField;
	@FXML Button restartConfigButton;
	@FXML Button cancelConfigButton;
	@FXML Button saveConfigButton;
	
	private static int xi = 60;
	private static int yi = 29;
	private static int xf = 60;
	private static int yf = 31;
	
	public ConfigurePaneController(){
		ImplementacionSolucion.firstNode = true;
		ImplementacionSolucion.posicionInicial = new Posicion(xi, yi);
		ImplementacionSolucion.posicionFinal = new Posicion(xf, yf);
	}
	
	@FXML
	private void initialize(){
		xiTextField.setText(xi + "");
		yiTextField.setText(yi + "");
		xfTextField.setText(xf + "");
		yfTextField.setText(yf + "");
	}

	/**
	 * @return the mainApp
	 */
	public Main getMainApp() {
		return mainApp;
	}

	/**
	 * @param mainApp the mainApp to set
	 */
	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
	}
	
	/**
	 * @return the dialogStage
	 */
	public Stage getDialogStage() {
		return dialogStage;
	}

	/**
	 * @param dialogStage the dialogStage to set
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
		this.dialogStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			public void handle(WindowEvent we){
				try {
					Alert close = new Alert(Alert.AlertType.CONFIRMATION);
					close.setHeaderText("Alerta...");
					close.setContentText("�Guardar Cambios?");
					
					close.showAndWait().ifPresent(response -> {
						if ( response == ButtonType.OK ) {
							handleSaveConfig();
						} else {
							we.consume();
						}
					});
				} catch (Exception e) {
					System.err.println(e);
				}
			}
		});
	}

	@FXML
	private void handleRestartConfig() {
		
	}
	
	@FXML
	private void handleCancelConfig() {
		this.dialogStage.close();
	}
	
	@FXML
	private void handleSaveConfig() {
		boolean success = true;
		StringBuffer buf = new StringBuffer();
		int x0 = 0;
		int y0 = 0;
		int x1 = 0;
		int y1 = 0;
		try { x0 = Integer.parseInt(xiTextField.getText().trim()); } 
		catch (NumberFormatException nfex) { 
			buf.append("Inv�lido para xi: ");
			buf.append('\"');
			buf.append(xiTextField.getText());
			buf.append('\"');
			buf.append('\n');
			xiTextField.setText("");
			success = false; 
		}
		try { y0 = Integer.parseInt(yiTextField.getText().trim()); } 
		catch (NumberFormatException nfex) { 
			buf.append("Inv�lido para yi: ");
			buf.append('\"');
			buf.append(yiTextField.getText());
			buf.append('\"');
			buf.append('\n');
			yiTextField.setText("");
			success = false; 
		}
		try { x1 = Integer.parseInt(xfTextField.getText().trim()); } 
		catch (NumberFormatException nfex) { 
			buf.append("Inv�lido para xf: ");
			buf.append('\"');
			buf.append(xfTextField.getText());
			buf.append('\"');
			buf.append('\n');
			xfTextField.setText("");
			success = false; 
		}
		try { y1 = Integer.parseInt(yfTextField.getText().trim()); } 
		catch (NumberFormatException nfex) { 
			buf.append("Inv�lido para yf: ");
			buf.append('\"');
			buf.append(yfTextField.getText());
			buf.append('\"');
			buf.append('\n');
			yfTextField.setText("");
			success = false; 
		}
			
		if(success){
			xi = x0;
			yi = y0;
			xf = x1;
			yf = y1;
			
			ImplementacionSolucion.posicionInicial = new Posicion(xi, yi);
			ImplementacionSolucion.posicionFinal = new Posicion(xf, yf);
			
			this.dialogStage.close();
		} else {
			Alert alerta = new Alert(Alert.AlertType.ERROR);
			alerta.setHeaderText("Error de Conversi�n");
			alerta.setContentText(buf.toString());
			alerta.showAndWait();
		}
		
	}

	public static Posicion getPosInit() {
		return new Posicion(xi, yi);
	}
	
	public static Posicion getPosEnd() {
		return new Posicion(xf, yf);
	}
}
