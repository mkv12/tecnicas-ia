package tecia.laberinto.view;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import tecia.laberinto.Main;

public class RootLayoutController {
	private Main mainApp;
	
	@FXML MenuItem optionConfigure;
	@FXML MenuItem optionToggleConsole;
	@FXML MenuItem optionClearConsole;
	@FXML MenuItem optionClose;
	@FXML MenuItem optionAbout;
	@FXML TextArea consoleArea;
	
	
	public RootLayoutController(){
		;
	}

	/**
	 * @return the mainApp
	 */
	public Main getMainApp() {
		return mainApp;
	}

	/**
	 * @param mainApp the mainApp to set
	 */
	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
	}
	
	@FXML
	public void handleConfigure(){
		sendToConsole("Option: Open configure option");
		this.mainApp.initConfigurePaneModal();
	}
	
	@FXML
	public void handleClose(){
		sendToConsole("Option: Closing Window");
		try {
			Alert close = new Alert(Alert.AlertType.CONFIRMATION);
			close.setContentText("�Desea Cerrar la Aplicaci�n?");
			
			close.showAndWait().ifPresent(response -> {
				if(response == ButtonType.OK){
					Platform.exit();
				}
			});
			
		} catch (Exception e) {
			sendToConsole(e.toString());
		}
	}
	
	@FXML
	public void handleAbout(){
		sendToConsole("Option: About us...");
	}
	
	public void sendToConsole(String msg){
		consoleArea.appendText(msg);
		consoleArea.appendText(" \n");
	}
	
	@FXML
	public void handleToggleConsole(){
		if(consoleArea.getHeight() <= 1.0)
			consoleArea.setPrefHeight(50.0);
		else
			consoleArea.setPrefHeight(1.0);
	}
	
	@FXML
	public void handleClearConsole() {
		consoleArea.setText("");
	}
}
