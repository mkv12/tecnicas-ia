package tecia.laberinto.control;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tecia.laberinto.model.Laberinto;
import tecia.laberinto.model.Operador;
import tecia.laberinto.model.Posicion;

public class ImplementacionSolucion {
	public static final int BPA = 1;
	public static final int BPP = 2;
	
	public static ObservableList<Posicion> opendedList = FXCollections.observableArrayList();
	public static ObservableList<Posicion> closedList = FXCollections.observableArrayList();
	public static ObservableList<Operador> operatorList = FXCollections.observableArrayList();
	
	public static Posicion posicionInicial 	= new Posicion();
	public static Posicion posicionFinal 	= new Posicion();
	public static Posicion posicionActual 	= new Posicion();
	
	public static Operador operadorActual = null;
	
	public static boolean firstNode = true;
	
	public static boolean PosInicialChanged = false;
	private static int metodoSolucion = BPP;
	
	public static void rellenarOperadoresBasicos(){
		operatorList.addAll(
				Laberinto.getUP(), 
				Laberinto.getDOWN(), 
				Laberinto.getLEFT(), 
				Laberinto.getRIGHT()
		);
	}
	
	public static boolean stepAllOperators() {
		boolean success = false; 
		try {
			operadorActual = operatorList.remove(0);
		} catch (IndexOutOfBoundsException iobex) {
			System.out.println("Se acabron los operadores. ");
			
			rellenarOperadoresBasicos();
			switch (metodoSolucion)
			{
				case BPA:
					posicionActual = opendedList.remove(0);
					closedList.add(0, posicionActual);
					PosInicialChanged = true;
					break;
				case BPP:
					posicionActual = opendedList.remove(opendedList.size() - 1);
					closedList.add(0, posicionActual);
					PosInicialChanged = true;
					break;
				default:
					break;
			}
			
			return success;
		}
		
		Posicion nwPos = null;
		try {
			nwPos = Laberinto.aplicaOperador(posicionActual, operadorActual);
		} catch ( Exception ex ) {
			System.err.println(ex);
		}
		
		if (nwPos == null) 
		{
			System.out.println("No se puede aplicar operador " + operadorActual);
		} 
		else if ( BuscarEn(opendedList, nwPos) ) 
		{
			System.out.println("Existe en Lista de Abiertos");
		}
		else if ( BuscarEn(closedList, nwPos) ) 
		{
			System.out.println("Existe en Lista de Cerrados");
		}
		else
		{
			posicionActual.getChildren().add(nwPos);
			
			opendedList.add(nwPos);
			
			if(operatorList.isEmpty())
			{
				rellenarOperadoresBasicos();
				switch (metodoSolucion)
				{
					case BPA:
						posicionActual = opendedList.remove(0);
						closedList.add(0, posicionActual);
						PosInicialChanged = true;
						break;
					case BPP:
						posicionActual = opendedList.remove(opendedList.size() - 1);
						closedList.add(0, posicionActual);
						PosInicialChanged = true;
						break;
					default:
						break;
				}
			}
			success = true;
		}
		
		return success;
	}

	private static boolean BuscarEn(ObservableList<Posicion> list, Posicion pos) {
		for (Posicion item : list) {
			if(item.comparar(pos)){
				return true;
			}
		}
		return false;
	}

	public static boolean esPosicionFinal() {
		return posicionFinal.comparar(posicionActual);
	}
	
	public static void establecerMetodoSolucion(int key) {
		switch (key) {
		case BPA:
			metodoSolucion = BPA;
			break;
		case BPP:
			metodoSolucion = BPP;
			break;
		default:
			metodoSolucion = BPA;
			break;
		}
	}
}