package tecia.laberinto;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import tecia.laberinto.view.ConfigurePaneController;
import tecia.laberinto.view.DrawLaberintoController;
import tecia.laberinto.view.NodeListController;
import tecia.laberinto.view.RootLayoutController;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

public class Main extends Application {
	private Stage rootStage;
	private BorderPane rootPane;
	private RootLayoutController controlRoot;
	private DrawLaberintoController controlDraw;
	private NodeListController controlNodeList;
	
	/**
	 * Constructor
	 * */
	public Main() {
		;
	}
	
	@Override
	public void start(Stage primaryStage) {
		this.rootStage = primaryStage;
		this.rootStage.setTitle("Laberinto");
		this.rootStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			public void handle(WindowEvent we){
				try {
					Alert close = new Alert(Alert.AlertType.CONFIRMATION);
					close.setHeaderText("Saliendo...");
					close.setContentText("�Desea Cerrar la Aplicaci�n?");
					
					close.showAndWait().ifPresent(response -> {
						if ( response == ButtonType.OK ) {
							Platform.exit();
						} else {
							we.consume();
						}
					});
				} catch (Exception e) {
					System.err.println(e);
				}
			}
		});
		
		initRootLayout();
		initDrawLaberintoLayout();
		initNodeListLayout();
		
		initConfigurePaneModal();
	}

	private void initRootLayout() {
		try {
			/* Load View */
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/RootLayout.fxml"));
			rootPane = (BorderPane) loader.load();
			
			Scene rootScene = new Scene(rootPane);
			rootStage.setScene(rootScene);
			
			/* Load Controller */
			controlRoot = loader.getController();
			controlRoot.setMainApp(this);
			
			rootStage.show();
			
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	private void initDrawLaberintoLayout(){
		try {
			/* Load View */
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/DrawLaberintoView.fxml"));
			AnchorPane anchorPane = (AnchorPane) loader.load();
			
			rootPane.setCenter(anchorPane);
			
			/*Load Controller*/
			controlDraw = loader.getController();
			controlDraw.setMainApp(this);
			/*Run after load*/
			controlDraw.dibujarLaberintoInicial();
			
		} catch(IOException ioe) {
			System.err.println(ioe);
		}
	}
	
	private void initNodeListLayout(){
		try {
			/* Load View */
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/NodeListView.fxml"));
			AnchorPane anchorPane = (AnchorPane) loader.load();
			
			rootPane.setLeft(anchorPane);
			
			/* Load Controller */
			controlNodeList = loader.getController();
			controlNodeList.setMainApp(this);
			
		} catch (IOException e) {
			System.err.println(e);
		}
	}
	
	public void initConfigurePaneModal(){
		try {
			/* Load View */
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/ConfigurePaneView.fxml"));
			AnchorPane anchorPane = (AnchorPane) loader.load();
			
			/* Prepare Modal */
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Configurar Aplicaci�n");
			dialogStage.initModality(Modality.APPLICATION_MODAL);
			dialogStage.initOwner(rootStage);
			dialogStage.setScene(new Scene(anchorPane));
			
			/* Load Controller */
			ConfigurePaneController control = loader.getController();
			control.setDialogStage(dialogStage);
			control.setMainApp(this);
			
			dialogStage.showAndWait();
			
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
	}

	/**
	 * @return the rootPane
	 */
	public BorderPane getRootPane() {
		return rootPane;
	}

	/**
	 * @param rootPane the rootPane to set
	 */
	public void setRootPane(BorderPane rootPane) {
		this.rootPane = rootPane;
	}
	
	/**
	 * Print a message on console GUI
	 * Root Node
	 * @param sMessage A string message
	 */
	public void toConsole(String sMessage){
		controlRoot.sendToConsole(sMessage);
	}
	
	/**
	 * Draw an step 
	 * Draw Node
	 * @param
	 */
	public void drawingStep(int i, int j) {
		controlDraw.drawStep(i, j);
	}
	
	/**
	 * Static Main Method
	 * @param args Arguments for Command Line
	 */
	public static void main(String[] args) {
		launch(args);
	}

	public void reiniciarLaberinto() {
		controlDraw.ReiniciarLaberinto();
	}

}
