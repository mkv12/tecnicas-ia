import java.util.ArrayList;
import java.util.Scanner;

public class StartMain {
	public static ArrayList<Posicion> ListaCerrados = new ArrayList<>(0);
	public static ArrayList<Posicion> ListaAbiertos = new ArrayList<>(0);
	
	public static Scanner scn = new Scanner(System.in);
	
	public static void main(String[] args) {
		NavegacionProfundo((byte)1, (byte) 5);
		//NavegacionAncho((byte)1, (byte) 5);
	}

	private static void NavegacionProfundo(byte _xi, byte _yi) {
		byte xi = _xi;
		byte yi = _yi;
		
		Posicion pos = null;
		Posicion n = null;
		/* Preparar Estructuras de Datos*/
		ListaAbiertos.clear();
		ListaCerrados.clear();
		
		ListaAbiertos.add(new Posicion(xi, yi));
		byte op_actual = (byte) 0;

		while(true){
			/* Condicion de Entrada. */
			if(Operador.ListaOperadores.isEmpty()){
				Operador.RellenarListaOperadores();
				pos = ListaAbiertos.remove(0);
				ListaCerrados.add(pos);
			}			
			/*Estado Actual*/
			System.out.println("******************************************************************");
			System.out.print("Actual:\t\t");
			System.out.println(pos);
			System.out.print("Abiertos:\t");
			System.out.println(ListaAbiertos);
			System.out.print("Cerrados:\t");
			System.out.println(ListaCerrados);
			System.out.print("Operadores:\t");
			System.out.println(Operador.listaOpToString());
			System.out.println(Mapa.imprimirMapa(pos, ListaCerrados, 0, 0));
			
			
			/*Condicion de Salida*/
			System.out.print(">>");
			String a = scn.nextLine();
			if(a.length() == 0){
				/*Continuar...*/
			}else if(a.charAt(0) == 'q'){
				System.out.println("**Fin de la Ejecucion**");
				break;
			}
			
			/*Obtener ultimo operador */
			op_actual = Operador.ListaOperadores.remove(0);
			/*Aplicar Operador*/
			n = Mapa.aplicaOperador(pos,op_actual);
			
			if(n == null){
				continue;
			}
		
			boolean b = BuscarEn(ListaAbiertos, n);
			boolean q = BuscarEn(ListaCerrados, n);
			
			if(b || q ){
				continue;
			}
			
			ListaAbiertos.add(n);
			pos.anadirAdyacente(n);
			
		};
	}

	private static void NavegacionAncho(byte _xi, byte _yi) {
		byte xi = _xi;
		byte yi = _yi;
		
		Posicion pos = null;
		Posicion n = null;
		/* Preparar Estructuras de Datos*/
		ListaAbiertos.clear();
		ListaCerrados.clear();
		
		ListaAbiertos.add(new Posicion(xi, yi));
		byte op_actual = (byte) 0;

		while(true){
			/* Condicion de Entrada. */
			if(Operador.ListaOperadores.isEmpty()){
				Operador.RellenarListaOperadores();
				pos = ListaAbiertos.remove(ListaAbiertos.size() - 1);
				ListaCerrados.add(pos);
			}			
			/*Estado Actual*/
			System.out.println("******************************************************************");
			System.out.print("Actual:\t\t");
			System.out.println(pos);
			System.out.print("Abiertos:\t");
			System.out.println(ListaAbiertos);
			System.out.print("Cerrados:\t");
			System.out.println(ListaCerrados);
			System.out.print("Operadores:\t");
			System.out.println(Operador.listaOpToString());
			System.out.println(Mapa.imprimirMapa(pos, ListaCerrados, 0, 0));
			
			/*Condicion de Salida*/
			System.out.print(">>");
			String a = scn.nextLine();
			if(a.length() == 0){
				/*Continuar...*/
			}else if(a.charAt(0) == 'q'){
				System.out.println("**Fin de la Ejecucion**");
				break;
			}
			
			/*Obtener ultimo operador */
			op_actual = Operador.ListaOperadores.remove(0);
			/*Aplicar Operador*/
			n = Mapa.aplicaOperador(pos,op_actual);
			
			if(n == null){
				continue;
			}
		
			boolean b = BuscarEn(ListaAbiertos, n);
			boolean q = BuscarEn(ListaCerrados, n);
			
			if(b || q ){
				continue;
			}
			
			ListaAbiertos.add(n);
			pos.anadirAdyacente(n);
		}
	}
	
	private static boolean BuscarEn(ArrayList<Posicion> lista, Posicion pos) {
		for (Posicion itemPos: lista) {
			if(itemPos.Compare(pos)){
				return true;
			}
		}
		
		return false;
	}	
}
