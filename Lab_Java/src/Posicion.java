import java.util.ArrayList;

public class Posicion {
	private byte posX;
	private byte posY;
	private ArrayList<Posicion> adyacente;
	
	public Posicion(){
		this.posX = 0;
		this.posY = 0;
		this.adyacente = new ArrayList<>(0);
	}

	public Posicion(byte _posX, byte _posY){
		this.posX = _posX;
		this.posY = _posY;
		this.adyacente = new ArrayList<>(0);
	}
	/**
	 * @return the posX
	 */
	public byte getPosX() {
		return posX;
	}

	/**
	 * @param posX the posX to set
	 */
	public void setPosX(byte posX) {
		this.posX = posX;
	}

	/**
	 * @return the posY
	 */
	public byte getPosY() {
		return posY;
	}

	/**
	 * @param posY the posY to set
	 */
	public void setPosY(byte posY) {
		this.posY = posY;
	}

	/**
	 * @return the adyacente
	 */
	public ArrayList<Posicion> getAdyacente() {
		return adyacente;
	}
	
	/**
	 * @param Posicion */
	public void anadirAdyacente(Posicion _ady){
		this.adyacente.add(_ady);
	}
	
	@Override
	public String toString() {
		return "(" + this.posX +","+this.posY+")";
	}

	public boolean Compare(Posicion pos) {
		return (this.posX == pos.posX && this.posY == pos.posY);
	}
}
