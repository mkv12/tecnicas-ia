import java.util.ArrayList;

public class Utilidades {
	public static String contenidoLista(ArrayList<Posicion> lista){
		StringBuffer salida = new StringBuffer();
		salida.append('{');
		for (int i = 0; i < lista.size(); i++) {
			Posicion pos = lista.get(i);
			if(pos == null){
				lista.remove(i); 
				continue;
			}
			salida.append(pos.toString());
			salida.append(',');
		}
		salida.append('}');
		return salida.toString();
	}
}
