import java.util.ArrayList;

public class Operador {
	/**
	 * Operadores
	 * */
	public static final byte UP 	= 1;
	public static final byte DOWN 	= 2;
	public static final byte LEFT 	= 4;
	public static final byte RIGHT 	= 8;
	
	public static ArrayList<Byte> ListaOperadores = new ArrayList<>();
	
	public static void RellenarListaOperadores(){
		ListaOperadores.clear();
		
		ListaOperadores.add(UP);
		ListaOperadores.add(DOWN);
		ListaOperadores.add(LEFT);
		ListaOperadores.add(RIGHT);
	}

	public static void RellenarListaOperadoresCon(Byte... params){
		ListaOperadores.clear();
		for (Byte item : params) {
			ListaOperadores.add(item);
		}
	}
	
	public static String listaOpToString(){
		StringBuffer buf = new StringBuffer();
		buf.append('[');
		for (int i = 0; i < ListaOperadores.size(); i++) {
			switch (ListaOperadores.get(i)) {
			case UP: buf.append('U');
				break;
			case DOWN: buf.append('D');
				break;
			case LEFT: buf.append('L');
				break;
			case RIGHT: buf.append('R');
			default:
				break;
			}
			buf.append(',');
		}
		buf.append(']');
		
		return buf.toString();
	}
}
